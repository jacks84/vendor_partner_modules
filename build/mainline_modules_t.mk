#
# Copyright 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Mainline configuration for regular devices that
#   are not low RAM and
#   can support updatable APEX
#
# Flags for partners:
#   MODULE_BUILD_FROM_SOURCE := true or false
#   - controls whether to build Mainline modules from source or not
#   MAINLINE_INCLUDE_UWB_MODULE := true or false
#   - when it is true, UWB module will be added to PRODUCT_PACKAGES
#   MAINLINE_INCLUDE_WIFI_MODULE := true or false
#   - when it is true, WiFi module will be added to PRODUCT_PACKAGES
#   (TODO)MAINLINE_INCLUDE_BLUETOOTH_MODULE := true or false
#   - (TODO)when it is true, Bluetooth module will be added to PRODUCT_PACKAGES
#   MAINLINE_COMPRESS_APEX_ALL := true or false
#   - when true, all the APEX modules will use compressed variant.
#   - when false, all the APEX modules will use uncompressed variant.
#   - if not defined, per-module option will be used
#   MAINLINE_COMPRESS_APEX_<module> := true or false
#   - per-module option that controls whether to use compresssed variant
#

# Mainline modules - APK type
PRODUCT_PACKAGES += \
    com.google.android.modulemetadata \
    DocumentsUIGoogle \
    CaptivePortalLoginGoogle \
    NetworkStackGoogle \
    com.google.mainline.telemetry \
    com.google.mainline.adservices \

# Ingesting networkstack.x509.pem
PRODUCT_MAINLINE_SEPOLICY_DEV_CERTIFICATES=vendor/partner_modules/build/certificates

# Overlay packages for APK-type modules
PRODUCT_PACKAGES += \
    GoogleDocumentsUIOverlay \
    ModuleMetadataGoogleOverlay \
    GooglePermissionControllerOverlay \
    GooglePermissionControllerFrameworkOverlay \
    GoogleExtServicesConfigOverlay \
    CaptivePortalLoginFrameworkOverlay \

# Configure APEX as updatable
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Mainline modules - APEX type
PRODUCT_PACKAGES += \
    com.google.mainline.primary.libs \
    com.google.android.tzdata4 \

# adding compressed APEX based on options

# MAINLINE_COMPRESS_APEX_ALL is for setting compression for all the modules
# When not defined, per-module options are used

ifeq ($(MAINLINE_COMPRESS_APEX_ALL),true)
MAINLINE_COMPRESS_APEX_ADBD := true
MAINLINE_COMPRESS_APEX_ADSERVICES := true
MAINLINE_COMPRESS_APEX_APPSEARCH := true
MAINLINE_COMPRESS_APEX_ART := true
MAINLINE_COMPRESS_APEX_BLUETOOTH := true
MAINLINE_COMPRESS_APEX_CELLBROADCAST := true
MAINLINE_COMPRESS_APEX_CONSCRYPT := true
MAINLINE_COMPRESS_APEX_RESOLV := true
MAINLINE_COMPRESS_APEX_EXTSERVICES := true
MAINLINE_COMPRESS_APEX_IPSEC := true
MAINLINE_COMPRESS_APEX_MEDIA := true
MAINLINE_COMPRESS_APEX_MEDIAPROVIDER := true
MAINLINE_COMPRESS_APEX_MEDIASWCODEC := true
MAINLINE_COMPRESS_APEX_NEURALNETWORKS := true
MAINLINE_COMPRESS_APEX_ONDEVICEPERSONALIZATION := true
MAINLINE_COMPRESS_APEX_STATSD := true
MAINLINE_COMPRESS_APEX_PERMISSION := true
MAINLINE_COMPRESS_APEX_SCHEDULING := true
MAINLINE_COMPRESS_APEX_SDKEXT := true
MAINLINE_COMPRESS_APEX_TETHERING := true
MAINLINE_COMPRESS_APEX_UWB := true
MAINLINE_COMPRESS_APEX_WIFI := true
else ifeq ($(MAINLINE_COMPRESS_APEX_ALL),false)
MAINLINE_COMPRESS_APEX_ADBD := false
MAINLINE_COMPRESS_APEX_ADSERVICES := false
MAINLINE_COMPRESS_APEX_APPSEARCH := false
MAINLINE_COMPRESS_APEX_ART := false
MAINLINE_COMPRESS_APEX_BLUETOOTH := false
MAINLINE_COMPRESS_APEX_CELLBROADCAST := false
MAINLINE_COMPRESS_APEX_CONSCRYPT := false
MAINLINE_COMPRESS_APEX_RESOLV := false
MAINLINE_COMPRESS_APEX_EXTSERVICES := false
MAINLINE_COMPRESS_APEX_IPSEC := false
MAINLINE_COMPRESS_APEX_MEDIA := false
MAINLINE_COMPRESS_APEX_MEDIAPROVIDER := false
MAINLINE_COMPRESS_APEX_MEDIASWCODEC := false
MAINLINE_COMPRESS_APEX_NEURALNETWORKS := false
MAINLINE_COMPRESS_APEX_ONDEVICEPERSONALIZATION := false
MAINLINE_COMPRESS_APEX_STATSD := false
MAINLINE_COMPRESS_APEX_PERMISSION := false
MAINLINE_COMPRESS_APEX_SCHEDULING := false
MAINLINE_COMPRESS_APEX_SDKEXT := false
MAINLINE_COMPRESS_APEX_TETHERING := false
MAINLINE_COMPRESS_APEX_UWB := false
MAINLINE_COMPRESS_APEX_WIFI := false
endif

# module_sdk and optional modules
MODULE_BUILD_FROM_SOURCE ?= false

SOONG_CONFIG_NAMESPACES += wifi_module
SOONG_CONFIG_wifi_module += source_build
SOONG_CONFIG_wifi_module_source_build := true

SOONG_CONFIG_NAMESPACES += uwb_module
SOONG_CONFIG_uwb_module += source_build
SOONG_CONFIG_uwb_module_source_build := true

#SOONG_CONFIG_NAMESPACES += bluetooth_module
#SOONG_CONFIG_bluetooth_module += source_build
#SOONG_CONFIG_bluetooth_module_source_build := true

# Adbd
MAINLINE_COMPRESS_APEX_ADBD ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_ADBD),true)
PRODUCT_PACKAGES += \
    com.google.android.adbd_trimmed_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.adbd_trimmed_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.adbd_trimmed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.adbd_trimmed.apex
endif

# AdServices
MAINLINE_COMPRESS_APEX_ADSERVICES ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_ADSERVICES),true)
PRODUCT_PACKAGES += \
    com.google.android.adservices_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.adservices_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.adservices
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.adservices.apex
endif

# AppSearch
MAINLINE_COMPRESS_APEX_APPSEARCH ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_APPSEARCH),true)
PRODUCT_PACKAGES += \
    com.google.android.appsearch_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.appsearch_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.appsearch
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.appsearch.apex
endif

# Art
MAINLINE_COMPRESS_APEX_ART ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_ART),true)
PRODUCT_PACKAGES += \
    com.google.android.art_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.art_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.art
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.art.apex
endif

# Optional Bluetooth
# TODO add these back after Bluetooth module is added
#MAINLINE_INCLUDE_BLUETOOTH_MODULE ?= false
#MAINLINE_COMPRESS_APEX_BLUETOOTH ?= true
#ifeq ($(MAINLINE_INCLUDE_BLUETOOTH_MODULE),true)
#SOONG_CONFIG_bluetooth_module_source_build := false
#ifeq ($(MAINLINE_COMPRESS_APEX_BLUETOOTH),true)
#PRODUCT_PACKAGES += \
#    com.google.android.bluetooth_compressed
#PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
#    system/apex/com.google.android.bluetooth_compressed.apex
#else
#PRODUCT_PACKAGES += \
#    com.google.android.bluetooth
#PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
#    system/apex/com.google.android.bluetooth.apex
#endif
#endif

# CellBroadcast
MAINLINE_COMPRESS_APEX_CELLBROADCAST ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_CELLBROADCAST),true)
PRODUCT_PACKAGES += \
    com.google.android.cellbroadcast_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.cellbroadcast_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.cellbroadcast
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.cellbroadcast.apex
endif

# Conscrypt
MAINLINE_COMPRESS_APEX_CONSCRYPT ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_CONSCRYPT),true)
PRODUCT_PACKAGES += \
    com.google.android.conscrypt_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.conscrypt_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.conscrypt
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.conscrypt.apex
endif

# DNS Resolver
MAINLINE_COMPRESS_APEX_RESOLV ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_RESOLV),true)
PRODUCT_PACKAGES += \
    com.google.android.resolv_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.resolv_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.resolv
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.resolv.apex
endif

# ExtServices - apex
MAINLINE_COMPRESS_APEX_EXTSERVICES ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_EXTSERVICES),true)
PRODUCT_PACKAGES += \
    com.google.android.extservices_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.extservices_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.extservices
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.extservices.apex
endif

# Ipsec
MAINLINE_COMPRESS_APEX_IPSEC ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_IPSEC),true)
PRODUCT_PACKAGES += \
    com.google.android.ipsec_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.ipsec_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.ipsec
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.ipsec.apex
endif

# Media
MAINLINE_COMPRESS_APEX_MEDIA ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_MEDIA),true)
PRODUCT_PACKAGES += \
    com.google.android.media_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.media_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.media
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.media.apex
endif

# MediaProvider
MAINLINE_COMPRESS_APEX_MEDIAPROVIDER ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_MEDIAPROVIDER),true)
PRODUCT_PACKAGES += \
    com.google.android.mediaprovider_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.mediaprovider_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.mediaprovider
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.mediaprovider.apex
endif

# MediaSwCodec
MAINLINE_COMPRESS_APEX_MEDIASWCODEC ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_MEDIASWCODEC),true)
PRODUCT_PACKAGES += \
    com.google.android.media.swcodec_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.media.swcodec_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.media.swcodec
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.media.swcodec.apex
endif

# Neural Networks
MAINLINE_COMPRESS_APEX_NEURALNETWORKS ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_NEURALNETWORKS),true)
PRODUCT_PACKAGES += \
    com.google.android.neuralnetworks_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.neuralnetworks_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.neuralnetworks
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.neuralnetworks.apex
endif

# OnDevicePersonalization
MAINLINE_COMPRESS_APEX_ONDEVICEPERSONALIZATION ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_ONDEVICEPERSONALIZATION),true)
PRODUCT_PACKAGES += \
    com.google.android.ondevicepersonalization_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.ondevicepersonalization_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.ondevicepersonalization
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.ondevicepersonalization.apex
endif

# Statsd
MAINLINE_COMPRESS_APEX_STATSD ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_STATSD),true)
PRODUCT_PACKAGES += \
    com.google.android.os.statsd_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.os.statsd_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.os.statsd
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.os.statsd.apex
endif

# Permission
MAINLINE_COMPRESS_APEX_PERMISSION ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_PERMISSION),true)
PRODUCT_PACKAGES += \
    com.google.android.permission_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.permission_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.permission
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.permission.apex
endif

# Scheduling
MAINLINE_COMPRESS_APEX_SCHEDULING ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_SCHEDULING),true)
PRODUCT_PACKAGES += \
    com.google.android.scheduling_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.scheduling_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.scheduling
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.scheduling.apex
endif

# SdkExtensions
MAINLINE_COMPRESS_APEX_SDKEXT ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_SDKEXT),true)
PRODUCT_PACKAGES += \
    com.google.android.sdkext_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.sdkext_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.sdkext
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.sdkext.apex
endif

# Tethering
MAINLINE_COMPRESS_APEX_TETHERING ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_TETHERING),true)
PRODUCT_PACKAGES += \
    com.google.android.tethering_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.tethering_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.tethering
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.tethering.apex
endif

# Optional Uwb
MAINLINE_INCLUDE_UWB_MODULE ?= false
MAINLINE_COMPRESS_APEX_UWB ?= true
ifeq ($(MAINLINE_INCLUDE_UWB_MODULE),true)
SOONG_CONFIG_uwb_module_source_build := false
ifeq ($(MAINLINE_COMPRESS_APEX_UWB),true)
PRODUCT_PACKAGES += \
    com.google.android.uwb_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.uwb_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.uwb
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.uwb.apex
endif
endif

# Optional WiFi
MAINLINE_INCLUDE_WIFI_MODULE ?= false
MAINLINE_COMPRESS_APEX_WIFI ?= true
ifeq ($(MAINLINE_INCLUDE_WIFI_MODULE),true)
SOONG_CONFIG_wifi_module_source_build := false
ifeq ($(MAINLINE_COMPRESS_APEX_WIFI),true)
PRODUCT_PACKAGES += \
    com.google.android.wifi_compressed
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.wifi_compressed.apex
else
PRODUCT_PACKAGES += \
    com.google.android.wifi
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.android.wifi.apex
endif
endif

# sysconfig files
PRODUCT_COPY_FILES += \
    vendor/partner_modules/build/google-staged-installer-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-staged-installer-whitelist.xml \

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/apex/com.google.mainline.primary.libs.apex \
    system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk \
    system/priv-app/NetworkStackGoogle/NetworkStackGoogle.apk \
    system/app/CaptivePortalLoginGoogle/CaptivePortalLoginGoogle.apk \
    system/etc/permissions/GoogleDocumentsUI_permissions.xml \
    system/etc/permissions/GoogleNetworkStack_permissions.xml \
    system/etc/sysconfig/preinstalled-packages-com.google.android.providers.media.module.xml \
    system/apex/com.google.android.tzdata4.apex \

ifeq (,$(findstring x86,$(TARGET_PRODUCT)))
# arm
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/framework/arm/%.art \
    system/framework/arm/%.oat \
    system/framework/arm/%.vdex \
    system/framework/arm64/%.art \
    system/framework/arm64/%.oat \
    system/framework/arm64/%.vdex \

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/framework/oat/arm/%.odex \
    system/framework/oat/arm/%.vdex \
    system/framework/oat/arm64/%.odex \
    system/framework/oat/arm64/%.vdex \

else
# x86
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/framework/x86/%.art \
    system/framework/x86/%.oat \
    system/framework/x86/%.vdex \
    system/framework/x86_64/%.art \
    system/framework/x86_64/%.oat \
    system/framework/x86_64/%.vdex \

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/framework/oat/x86/%.odex \
    system/framework/oat/x86/%.vdex \
    system/framework/oat/x86_64/%.odex \
    system/framework/oat/x86_64/%.vdex \

endif
